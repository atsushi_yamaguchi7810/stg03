
//scroll
//userAgent top scroll
var ua = navigator.userAgent;
if (ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0) {
  $(function() {
        var headerheight = 50;
        $('.logo a,.arrows a,nav a[href^="#"]').click(function() {
            var href = $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'html' : href);
            var position = target.offset().top - headerheight;
            $("html, body").animate({
                scrollTop: position
            }, 500, "swing");
            return false;
        });
    });
	
} else if (ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0) {
   $(function() {
        var headerheight = 160;
        $('.logo a,.arrows a,nav a[href^="#"]').click(function() {
            var href = $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'html' : href);
            var position = target.offset().top - headerheight;
            $("html, body").animate({
                scrollTop: position
            }, 500, "swing");
            return false;
        });
    });
} else {
 $(function() {
        var headerheight = 160;
        $('.logo a,.arrows a,nav a[href^="#"]').click(function() {
            var href = $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'html' : href);
            var position = target.offset().top - headerheight;
            $("html, body").animate({
                scrollTop: position
            }, 500, "swing");
            return false;
        });
    });
}

/*--------------------------------------
    page top	
  --------------------------------------*/
$(function(){
 var footerMenu = $("#page-top");
 footerMenu.hide();
 $(window).scroll(function(){
  if($(this).scrollTop() > 100){
   footerMenu.fadeIn();
  }else{
   footerMenu.fadeOut();
  }
 });

 var linkToTop = $("#linkPagetop");
 linkToTop.click(function(){
  $("body, html").animate({
   scrollTop:0
  }, 500);
  return false;
 });
});

$(function() {
/*--------------------------------------
    side navi
  --------------------------------------*/	
	 $(document).ready(function() {
        $.slidebars({
            siteClose: true // true or false
        });
    });
    
});
