  /*--------------------------------------
    AOS
  --------------------------------------*/   
$(function(){
    AOS.init({
        easing: 'easeOutExpo',
        duration: 1800
    });
});

$(function() {

/*--------------------------------------
    header　追従
  --------------------------------------*/		
	
      $("#header").sticky({ topSpacing: 0, center:true, className:"aha" });
    
});
/*--------------------------------------
    loader
  --------------------------------------*/	
$(function() {
  var h = $(window).height();
 
  $('#container').css('display','none');
  $('#loader-bg ,#loader').height(h).css('display','block');
});
 
$(window).load(function () { //全ての読み込みが完了したら実行
  $('#loader-bg').delay(900).fadeOut(800);
  $('#loader').delay(600).fadeOut(300);
  $('#container').css('display', 'block');
});
 
//10秒たったら強制的にロード画面を非表示
$(function(){
  setTimeout('stopload()',10000);
});
 
function stopload(){
  $('#container').css('display','block');
  $('#loader-bg').delay(900).fadeOut(800);
  $('#loader').delay(600).fadeOut(300);
}
