$(window).on('load',function(){

	/*========================================
		modal
	========================================*/

			$('div#modal').velocity('transition.fadeOut',{duration:500})
			$('html').css({'overflow-y':'scroll'});
			$('#top div.keyvisual div.cover1').velocity('transition.slideLeftBigIn',{delay:500,duration:1000})
			$('#top div.keyvisual div.pop1').velocity('transition.fadeIn',{delay:500,duration:1000})
			$('#top div.keyvisual p.detail').velocity('transition.fadeIn',{delay:500,duration:1000})
			$('#top div.btnAbout').velocity('transition.fadeIn',{delay:500,duration:1000})
			$('#top div.keyvisual div.cover2').velocity('transition.slideLeftBigIn',{delay:1000,duration:1000})

			$('#top div.keyvisual h1').velocity('transition.slideDownBigIn',{delay:1500,duration:1000})
			$('#top div.keyvisual p.catch').velocity('transition.fadeIn',{delay:1500,duration:1000})
			$('#top div.keyvisual p.catch2').velocity('transition.fadeIn',{delay:1500,duration:1000})
			$('#top div.btnWrap').velocity('transition.fadeIn',{delay:1500,duration:1000})

			$('#top div.keyvisual div.pop2').velocity('transition.fadeIn',{delay:2000,duration:1000})


	$('div#modal').click(function(){
			$('div#modal').velocity('transition.fadeOut',{duration:500})
			$('html').css({'overflow-y':'scroll'});
	});

	$('div.btnAbout,div.keyvisual-sp').click(function(){
		$('div#modal').velocity('transition.fadeIn',{duration:500})
		$('html').css({'overflow-y':'hidden'});
	});

	/*========================================
		viewer
	========================================*/
	
	$('#viewer .mainImage li').zoomer();
	$('#viewer .mainImage li').velocity('transition.fadeOut',{duration:10})
	$('#viewer .mainImage li:first-child').velocity('transition.fadeIn',{duration:10})
	
	var nav = $('#viewer .viewerThumb');
	var currentSlide = 1;
	var flg = 0;
	
	$('li',nav).on('click', function(){
		if (flg == 0){
			currentSlide = $('li',nav).index(this) + 1;
			flg = 1;
			$('#viewer .mainImage li').velocity('transition.fadeOut',{duration:100})
			$('#viewer .mainImage li:nth-child(' + currentSlide + ')').velocity('transition.fadeIn',{duration:200,complete: function(){
				flg = 0;
			}});
			$('#viewer .viewerThumb li').removeClass('current');
			$('#viewer .viewerThumb li:nth-child(' + currentSlide + ')').addClass('current');
		} else {
			console.log(false);
			return false;
		}
	});
	
	$('#viewer div.left').on('click', function(){
		if (flg == 0){
			if (currentSlide == 1){
				currentSlide = 8;
			} else {
				currentSlide = currentSlide - 1;
			}
			flg = 1;
			$('#viewer .mainImage li').velocity('transition.fadeOut',{duration:100})
			$('#viewer .mainImage li:nth-child(' + currentSlide + ')').velocity('transition.fadeIn',{duration:200,complete: function(){
				flg = 0;
			}});
			$('#viewer .viewerThumb li').removeClass('current');
			$('#viewer .viewerThumb li:nth-child(' + currentSlide + ')').addClass('current');
		} else {
			console.log(false);
			return false;
		}
	});
	
	$('#viewer div.right').on('click', function(){
		if (flg == 0){
			if (currentSlide == 8){
				currentSlide = 1;
			} else {
				currentSlide = currentSlide + 1;
			}
			flg = 1;
			$('#viewer .mainImage li').velocity('transition.fadeOut',{duration:100})
			$('#viewer .mainImage li:nth-child(' + currentSlide + ')').velocity('transition.fadeIn',{duration:200,complete: function(){
				flg = 0;
			}});
			$('#viewer .viewerThumb li').removeClass('current');
			$('#viewer .viewerThumb li:nth-child(' + currentSlide + ')').addClass('current');
		} else {
			console.log(false);
			return false;
		}
	});

	/*========================================
		scroll
	========================================*/
	
	$('#header nav.globalNav ul li a , div.btnWrap div a , #footer div.logo a ').click(function(){
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$("html, body").animate({scrollTop:position}, 500, "swing");
		return false;
	});
	
	/*========================================
		sideNav
	========================================*/
	
	$(function() {
	   var navLink = $('#content nav.sideNav ul li a');
	 
	   var contentsArr = new Array();
	  for (var i = 0; i < navLink.length; i++) {
	      var targetContents = navLink.eq(i).attr('href');
	      if(targetContents.charAt(0) == '#') {
	            var targetContentsTop = $(targetContents).offset().top;
	            var targetContentsBottom = targetContentsTop + $(targetContents).outerHeight(true) - 1;
	            contentsArr[i] = [targetContentsTop, targetContentsBottom]
	      }
	   };
	 
	   function currentCheck() {
	        var windowScrolltop = $(window).scrollTop();
	        for (var i = 0; i < contentsArr.length; i++) {
	          if(contentsArr[i][0] <= windowScrolltop && contentsArr[i][1] >= windowScrolltop) {
	               navLink.removeClass('current');
	               navLink.eq(i).addClass('current');
	                i == contentsArr.length;
	            }
	       };
	  }
	 
	  $(window).on('load scroll', function() {
	      currentCheck();
	 });
	 
	    navLink.click(function() {
	      $('html,body').animate({
	          scrollTop: $($(this).attr('href')).offset().top
	       }, 500);
	        return false;
	   })
	});
	
	/*========================================
		preload
	========================================*/
	
	function mypreload() {
	    for(var i = 0; i< arguments.length; i++){
	        $("<img>").attr("src", arguments[i]);
	    }
	}
	mypreload(
	'common/img/sidenav-bg1_on@2x.png',
	'common/img/sidenav-bg1@2x.png',
	'common/img/sidenav-bg2_on@2x.png',
	'common/img/sidenav-bg2@2x.png',
	'common/img/sidenav-bg3_on@2x.png',
	'common/img/sidenav-bg4_on@2x.png',
	'common/img/sidenav-bg5_on@2x.png',
	'common/img/sidenav-bg6_on@2x.png',
	'common/img/sidenav-bg6@2x.png');
	
	/*========================================
		accordion
	========================================*/
	
	$('#about ul.aboutList li h3').on("click", function() {
		$(this).next().slideToggle();
		$(this).toggleClass("active");
	});
	
});