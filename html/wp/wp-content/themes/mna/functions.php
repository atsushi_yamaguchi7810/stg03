<?php 

add_action( 'parse_query', 'my_parse_query' );
function my_parse_query( $query ) {
if ( ! isset( $query->query_vars['paged'] ) && isset( $query->query_vars['page'] ) )
$query->query_vars['paged'] = $query->query_vars['page'];
}
register_post_type(
	'live',
	array(
		'label' => 'ライブ',
		'menu_position' => 4,
		'hierarchical' => false,
		'public' => true,
		'has_archive' => true,
		'supports' => array(
		'title',
		'tags',
		'thumbnail'
		)
	)
	);
//カテゴリータイプ
$args = array(
'label' => 'カテゴリー',
'public' => true,
'show_ui' => true,
'hierarchical' => true
);
register_taxonomy('live_category','live',$args);

function show_page_number() {
	global $wp_query;

	$max_page = $wp_query->max_num_pages;

	return $max_page;
}




remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');



function my_form_tag_filter($tag){
  if ( ! is_array( $tag ) )
  return $tag;

  if(isset($_GET['date'])){
    $name = $tag['name'];
    if($name == 'your-goods')
      $tag['values'] = (array) $_GET['date'];
  }
  return $tag;
}
add_filter('wpcf7_form_tag', 'my_form_tag_filter', 11);
?>
<?php add_theme_support( 'post-thumbnails' ); ?>