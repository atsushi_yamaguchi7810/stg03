<?php 
/*
Template Name: page-top2019
*/

get_header(); ?>

<body id="top" class="ver2019_02">
<div id="loader-bg">
  <div id="loader"><img src="/assets/images/logo.png" alt="MONO NO AWARE"/></div>
</div>

<div id="container">
<section id="main" class="sec01">
  <h1><img src="/assets/images/logo.png" walt="MONO NO AWARE"/></h1>
  <div class="arrows"><a href="#movie"><img src="/assets/images/arrows.png" width="56" height="24" alt=""/></a></div>
</section>

<header id="header">
<div class="inner">
	<p class="logo"><a href="#top"><img src="/assets/images/logo_s.png" alt="MONO NO AWARE"/></a></p>
  <div class="menu sb-toggle-right navbar-right hover"></div>
  <nav class="main_navi sb-slidebar sb-right sb-style-overlay">
    <ul class="navi">
		<li><a href="#news" class="sb-close"><span><img src="/assets/images/ttl_icon_news.svg" alt=""/></span><em></em></a></li>
      <li><a href="#live" class="sb-close"><span><img src="/assets/images/ttl_icon_live.svg" alt=""/></span><em></em></a></li>
      <li><a href="#profile" class="sb-close"><span><img src="/assets/images/ttl_icon_profile02.svg" alt=""/></span><em></em></a></li>
      <li><a href="#disco" class="sb-close"><span><img src="/assets/images/ttl_icon_disco.svg" alt=""/></span><em></em></a></li>
      <li><a href="#contact" class="sb-close"><span><img src="/assets/images/ttl_icon_contact.svg" alt=""/></span><em></em></a></li>
    </ul>
    <ul class="snsArea">
        <li><a href="https://open.spotify.com/artist/5vFyh7GL35ShoJWrXL9aUR?si=NSvHVlZ1TmGNy5vRS-q0yQ" target="_blank"><img src="/assets/images/icon01.png" alt="spotify"/></a></li>
        <li><a href="https://itunes.apple.com/jp/artist/mono-no-aware/1204011959" target="_blank"><img src="/assets/images/icon02.png" alt="apple music"/></a></li>
        <li><a href="https://www.instagram.com/mono.no.aware.0630/" target="_blank"><img src="/assets/images/icon03.png" alt="instagram"/></a></li>
        <li><a href="https://twitter.com/mono_no_aware_" target="_blank"><img src="/assets/images/icon04.png" alt="twitter"/></a></li>
        <li><a href="https://www.youtube.com/user/spaceshowermusic" target="_blank"><img src="/assets/images/icon05.png" alt="youtube"/></a></li>
      </ul>
  </nav>
	</div>
</header>
 
 
 
  <section id="movie" class="sec02" data-aos="fade-up" data-aos-once="true">
    <div class="inner">
      <div class="movie">
      <ul>
		  <li><iframe width="560" height="315" src="https://www.youtube.com/embed/UzACD_m_ZfA?rel=0&amp;controls=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></li>
		  <li><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/BzJGdIlUOdc?rel=0&amp;controls=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></li>
		  </ul>
      </div>
      <ul class="snsArea">
        <li><a href="https://open.spotify.com/artist/5vFyh7GL35ShoJWrXL9aUR?si=NSvHVlZ1TmGNy5vRS-q0yQ" target="_blank"><img src="/assets/images/icon01.png" alt="spotify"/></a></li>
        <li><a href="https://itunes.apple.com/jp/artist/mono-no-aware/1204011959" target="_blank"><img src="/assets/images/icon02.png" alt="apple music"/></a></li>
        <li><a href="https://www.instagram.com/mono.no.aware.0630/" target="_blank"><img src="/assets/images/icon03.png" alt="instagram"/></a></li>
        <li><a href="https://twitter.com/mono_no_aware_" target="_blank"><img src="/assets/images/icon04.png" alt="twitter"/></a></li>
        <li><a href="https://www.youtube.com/user/spaceshowermusic" target="_blank"><img src="/assets/images/icon05.png" alt="youtube"/></a></li>
      </ul>
    </div>
  </section>
  
  <section id="news" class="sec03">
    <div class="inner">
      <h2 data-aos="fade-up" data-aos-once="true"><img src="/assets/images/ttl_icon_news.svg" alt="NEWS"/></h2>
      <ul>
       <?php $args = array(
  'posts_per_page'   => 6 ); ?>
			<?php
			//条件のセット
			$the_query = new WP_Query( $args );
			?>
			<?php if ( $the_query->have_posts() ) : ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <li data-aos="fade-up" data-aos-once="true"><a href="<?php the_permalink(); ?>"><span><?php the_post_thumbnail('large'); ?></span>
          <figcaption>
            <h3><?php echo get_post_time('Y.m.d'); ?><br>
<?php the_title(); ?></h3>
            <!--<p><?php
if(mb_strlen($post->post_content,'UTF-8')>50){
	$content= str_replace('\n', '', mb_substr(strip_tags($post-> post_content), 0, 50,'UTF-8'));
	echo $content.'……';
}else{
	echo str_replace('\n', '', strip_tags($post->post_content));
}
?></p>-->
          </figcaption>
        </a></li>
        <?php endwhile; wp_reset_postdata(); ?>
			<?php else:  ?>
			<p>
				<?php _e( '投稿がありません。' ); ?>
			</p>
			<?php endif; ?>
      </ul>
      <p class="btn" data-aos="fade-up" data-aos-once="true"><a href="<?php bloginfo('url'); ?>/news/"><img src="/assets/images/bt_more.png" alt=""/><span>MORE</span></a></p>
    </div>
  </section>
  
  <section id="live" class="sec04">
    <div class="inner">
      <h2 data-aos="fade-up" data-aos-once="true"><img src="/assets/images/ttl_icon_live.svg" alt="LIVE"/></h2>
      <ul>
       <?php
					$args = array(
						'tax_query' => array(
							array(
								'taxonomy' => 'live_category',
								'field' => 'slug',
								'terms' => array( 'future_live' )
							)
						),
						'post_type' => 'live',
						'posts_per_page' => -3
					);
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ): $loop->the_post();
					?>
        <li data-aos="fade-up" data-aos-once="true">
			<div class="live_detail_inner">
         
         <?php
								$live_pic = get_post_meta( $post->ID, 'live_pic', true );
								$large = wp_get_attachment_image_src( $live_pic, 'large' );
								?>
								<?php if ( $large  ) : ?>
			<p class="pic"><a href="<?php the_permalink(); ?>"><img src="<?php echo $large[0];?>" alt=""></a></p>
								<?php endif;?>
          <div>
			  <?php if(post_custom('live_status')): ?>
			  <p class="status"><?php echo post_custom('live_status'); ?></p>
			  <?php endif; ?>
            <p class="ttl"><?php the_title(); ?></p>
            <p class="schedule"><?php echo $post->live_day; ?></p>
            <p class="venue"><?php echo $post->live_place; ?></p>
            <?php if(post_custom('live_open')): ?>
<p class="time"><?php echo post_custom('live_open'); ?></p>
         <?php endif; ?>
         <?php if(post_custom('live_artist_band')): ?>
<p><?php echo post_custom('live_artist_band'); ?></p>
         <?php endif; ?>
          </div>
			</div>
        </li>
        <?php endwhile; ?>
      </ul>
      <p class="btn" data-aos="fade-up" data-aos-once="true"><a href="<?php bloginfo('url'); ?>/live_schedule/"><img src="/assets/images/bt_more.png" alt=""/><span>MORE</span></a></p>
    </div>
  </section>
  
  <section id="profile" class="sec05">
    <div class="inner">
      <h2 data-aos="fade-up" data-aos-once="true"><img src="/assets/images/ttl_icon_profile.svg" alt="PROFILE"/></h2>
      <p class="pic" data-aos="fade-up" data-aos-once="true"><img src="/assets/images/main_v_2019_02.jpg" alt=""/></p>
      <p data-aos="fade-up" data-aos-once="true">東京都八丈島出身の玉置周啓、加藤成順は、大学で竹田綾子、柳澤豊に出会った。<br>
<br>
その結果、ポップの土俵にいながらも、多彩なバックグラウンド匂わすサウンド、期待を裏切るメロディライン、言葉遊びに長けた歌詞で、ジャンルや国内外の枠に囚われない自由な音を奏でるのだった。<br>
<br>
2017年3月には、1stアルバム「人生、山おり谷おり」をP-VINEより全国流通。<br>
同年8月ペトロールズのカヴァーEP「WHERE, WHO, WHAT IS PETROLZ?? - EP」に参加。<br>
同年12月には「人生、山おり谷おり」LP盤＋ZINEをリリース。<br>
渋谷WWWにて発売記念イベント「天下一舞踏会」を開催しチケットはソールドアウト。<br>
<br>
FUJI ROCK FESTIVAL’16 "ROOKIE A GO-GO"の投票から翌年のメインステージに出演。デビューアルバム「人生、山おり谷おり」が各方面から高い評価を得る中で、VIVA LA ROCK 2018、BAYCAMP等、数々の国内フェスに出演、The CribsやFazerdaze等の海外アーティストのサポートアクトを務めるなど、次世代のロックバンドとして注目を集める。
</p>
    </div>
  </section>
  
  <section id="disco" class="sec06">
    <div class="inner">
      <h2 data-aos="fade-up" data-aos-once="true"><img src="/assets/images/ttl_icon_disco.svg" alt=""/></h2>
      <ul>
       <li data-aos="fade-up" data-aos-once="true">
          <p class="pic"><img src="/assets/images/disco02.jpg" alt=""/></p>
		   <div><p class="ttl">2nd album『AHA』</p>
            <p class="info">発売日：2018年8月1日<br>
				定価：2,315円＋税</p>
            <p class="tl">
				<span>Track List</span><br>
            01. 東京<br>
02. 機関銃を撃たせないで<br>
03. そういう日もある<br>
04. DUGHNUTS<br>
05. 轟々雷音<br>
06. ひと夏の恋<br>
07. 勇敢なあの子<br>
08. かごめかごめ<br>
09. 窓<br>
				10. センチメンタル・ジャーニー</p>
       </div>
        </li>
        <li data-aos="fade-up" data-aos-once="true">
          <p class="pic"><img src="/assets/images/disco03.jpg" alt=""/></p>
			<div><p class="ttl">LP+ZINE『人生、山おり谷おり』</p>
            <p class="info">発売日：2017年12月20日<br>
				定価：3,750円＋税</p>
           <p class="tl">
			   <span>Track List</span><br>
A1. 井戸育ち<br>
A2. わかってるつもり<br>
A3. me to me<br>
A4. ブーゲンビリア<br>
A5. 夢の中で<br>
B1. マンマミーヤ！<br>
B2. イワンコッチャナイ<br>
B3. to(gen)kyo<br>
B4. 明日晴れたら<br>
B5. 駆け落ち</div>
        </li>
        <li data-aos="fade-up" data-aos-once="true">
          <p class="pic"><img src="/assets/images/disco01.png" alt=""/></p>
			<div><p class="ttl">1st album『人生、山おり谷おり』</p>
            <p class="info">発売日：2017年3月2日<br>
				定価：1,800円＋税</p>
           <p class="tl">
			   <span>Track List</span><br>
            01. 井戸育ち<br>
            02. マンマミーヤ！<br>
            03. わかってるつもり<br>
            04. イワンコッチャナイ<br>
            05. me to me<br>
            06. To(gen)kyo<br>
            07. ブーゲンビリア<br>
            08. 明日晴れたら<br>
            09. 夢の中で<br>
            10. 駈け落ち</div>
        </li>
      </ul>
    </div>
  </section>
  
  <section id="contact" class="sec07" data-aos="fade-up" data-aos-once="true">
    <div class="inner">
      <h2><img src="/assets/images/ttl_icon_contact.svg" alt=""/></h2>
      <p><span>※</span>は必須項目になります。</p>
      <?php echo do_shortcode( '[contact-form-7 id="4" title="コンタクトフォーム 1"]' ); ?>
      </div>
  </section>


		<?php get_footer(); ?>