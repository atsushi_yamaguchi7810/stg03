<?php 
/*
Template Name: page-news
*/

get_header("sub"); ?>

<body id="top" class="sub">

<div id="container">

<header id="header">
<div class="inner">
	<p class="logo"><a href="<?php bloginfo('url'); ?>/"><img src="/assets/images/logo_s.png" alt="MONO NO AWARE"/></a></p>
  <div class="menu sb-toggle-right navbar-right hover"></div>
  <nav class="main_navi sb-slidebar sb-right sb-style-overlay">
    <ul class="navi">
		<li><a href="<?php bloginfo('url'); ?>/news/" class="sb-close"><span><img src="/assets/images/ttl_icon_news.svg" alt=""/></span><em></em></a></li>
      <li><a href="<?php bloginfo('url'); ?>/live_schedule/" class="sb-close"><span><img src="/assets/images/ttl_icon_live.svg" alt=""/></span><em></em></a></li>
      <li><a href="<?php bloginfo('url'); ?>/#profile" class="sb-close"><span><img src="/assets/images/ttl_icon_profile02.svg" alt=""/></span><em></em></a></li>
      <li><a href="<?php bloginfo('url'); ?>/#disco" class="sb-close"><span><img src="/assets/images/ttl_icon_disco.svg" alt=""/></span><em></em></a></li>
      <li><a href="<?php bloginfo('url'); ?>/#contact" class="sb-close"><span><img src="/assets/images/ttl_icon_contact.svg" alt=""/></span><em></em></a></li>
    </ul>
    <ul class="snsArea">
        <li><a href="https://open.spotify.com/artist/5vFyh7GL35ShoJWrXL9aUR?si=NSvHVlZ1TmGNy5vRS-q0yQ" target="_blank"><img src="/assets/images/icon01.png" alt="spotify"/></a></li>
        <li><a href="https://itunes.apple.com/jp/artist/mono-no-aware/1204011959" target="_blank"><img src="/assets/images/icon02.png" alt="apple music"/></a></li>
        <li><a href="https://www.instagram.com/mono.no.aware.0630/" target="_blank"><img src="/assets/images/icon03.png" alt="instagram"/></a></li>
        <li><a href="https://twitter.com/mono_no_aware_" target="_blank"><img src="/assets/images/icon04.png" alt="twitter"/></a></li>
        <li><a href="https://www.youtube.com/user/spaceshowermusic" target="_blank"><img src="/assets/images/icon05.png" alt="youtube"/></a></li>
      </ul>
  </nav>
	</div>
</header>
 
  <section id="live" class="sec04">
    <div class="inner">
      <h2><img src="/assets/images/ttl_icon_news.svg" alt="NEWS"/></h2>
      <ul>
       <?php $args = array(
  'posts_per_page'   => 1000 ); ?>
			<?php
			//条件のセット
			$the_query = new WP_Query( $args );
			?>
			<?php if ( $the_query->have_posts() ) : ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <li>
			<div class="live_detail_inner">
          <p class="pic"><?php the_post_thumbnail('large'); ?></p>
          <div>
			  <p><?php echo get_post_time('Y.m.d'); ?></p>
            <p class="ttl"><?php the_title(); ?></p>
            <p class="schedule"><?php the_content(); ?></p>
          </div>
        <?php endwhile; wp_reset_postdata(); ?>
			<?php else:  ?>
			<p>
				<?php _e( '投稿がありません。' ); ?>
			</p>
			<?php endif; ?>
			</div>
		  </li>
      </ul>
    </div>
  </section>
  
  

		<?php get_footer(); ?>