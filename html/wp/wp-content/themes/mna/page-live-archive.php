<?php
/*
Template Name: page-past_live
*/

get_header( "sub" );
?>

<body id="top" class="sub">

	<div id="container">

		<header id="header">
			<div class="inner">
				<p class="logo"><a href="<?php bloginfo('url'); ?>/"><img src="/assets/images/logo_s.png" alt="MONO NO AWARE"/></a>
				</p>
				<div class="menu sb-toggle-right navbar-right hover"></div>
				<nav class="main_navi sb-slidebar sb-right sb-style-overlay">
					<ul class="navi">
						<li><a href="<?php bloginfo('url'); ?>/news/" class="sb-close"><span><img src="/assets/images/ttl_icon_news.svg" alt=""/></span><em></em></a>
						</li>
						<li><a href="<?php bloginfo('url'); ?>/live_schedule/" class="sb-close"><span><img src="/assets/images/ttl_icon_live.svg" alt=""/></span><em></em></a>
						</li>
						<li><a href="<?php bloginfo('url'); ?>/#profile" class="sb-close"><span><img src="/assets/images/ttl_icon_profile02.svg" alt=""/></span><em></em></a>
						</li>
						<li><a href="<?php bloginfo('url'); ?>/#disco" class="sb-close"><span><img src="/assets/images/ttl_icon_disco.svg" alt=""/></span><em></em></a>
						</li>
						<li><a href="<?php bloginfo('url'); ?>/#contact" class="sb-close"><span><img src="/assets/images/ttl_icon_contact.svg" alt=""/></span><em></em></a>
						</li>
					</ul>
					<ul class="snsArea">
						<li><a href="https://open.spotify.com/artist/5vFyh7GL35ShoJWrXL9aUR?si=NSvHVlZ1TmGNy5vRS-q0yQ" target="_blank"><img src="/assets/images/icon01.png" alt="spotify"/></a>
						</li>
						<li><a href="https://itunes.apple.com/jp/artist/mono-no-aware/1204011959" target="_blank"><img src="/assets/images/icon02.png" alt="apple music"/></a>
						</li>
						<li><a href="https://www.instagram.com/mono.no.aware.0630/" target="_blank"><img src="/assets/images/icon03.png" alt="instagram"/></a>
						</li>
						<li><a href="https://twitter.com/mono_no_aware_" target="_blank"><img src="/assets/images/icon04.png" alt="twitter"/></a>
						</li>
						<li><a href="https://www.youtube.com/user/spaceshowermusic" target="_blank"><img src="/assets/images/icon05.png" alt="youtube"/></a>
						</li>
					</ul>
				</nav>
			</div>
		</header>

		<section id="live" class="sec04">
			<div class="inner">
				<h2><img src="/assets/images/ttl_icon_live.svg" alt="LIVE"/></h2>
				<h3 class="sub_title">PAST LIVE</h3>
				<ul>
					<?php
					$args = array(
						'tax_query' => array(
							array(
								'taxonomy' => 'live_category',
								'field' => 'slug',
								'terms' => array( 'past_live' )
							)
						),
						'post_type' => 'live',
						'posts_per_page' => -100
					);
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ): $loop->the_post();
					?>
					<li>
						<div class="live_detail_inner">
							<?php
							$live_pic = get_post_meta( $post->ID, 'live_pic', true );
							$large = wp_get_attachment_image_src( $live_pic, 'large' );
							?>
							<?php if ( $large  ) : ?>
							<p class="pic"><img src="<?php echo $large[0];?>" alt="">
							</p>
							<?php endif;?>
							<div>
								<?php if(post_custom('live_status')): ?>
								<p class="status">
									<?php echo post_custom('live_status'); ?>
								</p>
								<?php endif; ?>
								<p class="ttl">
									<?php the_title(); ?>
								</p>
								<p class="schedule">
									<?php echo $post->live_day; ?>
								</p>
								<p class="venue">
									<?php echo $post->live_place; ?>
								</p>

								<?php if(post_custom('live_open')): ?>
								<p class="time">
									<?php echo post_custom('live_open'); ?>
								</p>
								<?php endif; ?>

								<?php if(post_custom('live_artist_band')): ?>
								<p class="artist">
									<?php echo post_custom('live_artist_band'); ?>
								</p>
								<?php endif; ?>

								<?php if(post_custom('live_playguide')): ?>
								<p>
									<?php echo post_custom('live_playguide'); ?>
								</p>
								<?php endif; ?>

								<?php if(post_custom('site_url')): ?>
								<p>
									<a href="<?php echo post_custom('site_url'); ?>">
										<?php echo post_custom('site_url'); ?>
									</a>
								</p>
								<?php endif; ?>

							</div>
						</div>
					</li>
					<?php endwhile; ?>
				</ul>
			</div>
		</section>

		<?php get_footer(); ?>