<?php 
/*
Template Name: page-live
*/

get_header("sub"); ?>

<body id="top" class="sub">

<div id="container">

<header id="header">
<div class="inner">
	<p class="logo"><a href="<?php bloginfo('url'); ?>/"><img src="/assets/images/logo_s.png" alt="MONO NO AWARE"/></a></p>
  <div class="menu sb-toggle-right navbar-right hover"></div>
  <nav class="main_navi sb-slidebar sb-right sb-style-overlay">
    <ul class="navi">
		<li><a href="<?php bloginfo('url'); ?>/news/" class="sb-close"><span><img src="/assets/images/ttl_icon_news.svg" alt=""/></span><em></em></a></li>
      <li><a href="<?php bloginfo('url'); ?>/live_schedule/" class="sb-close"><span><img src="/assets/images/ttl_icon_live.svg" alt=""/></span><em></em></a></li>
      <li><a href="<?php bloginfo('url'); ?>/#profile" class="sb-close"><span><img src="/assets/images/ttl_icon_profile02.svg" alt=""/></span><em></em></a></li>
      <li><a href="<?php bloginfo('url'); ?>/#disco" class="sb-close"><span><img src="/assets/images/ttl_icon_disco.svg" alt=""/></span><em></em></a></li>
      <li><a href="<?php bloginfo('url'); ?>/#contact" class="sb-close"><span><img src="/assets/images/ttl_icon_contact.svg" alt=""/></span><em></em></a></li>
    </ul>
    <ul class="snsArea">
        <li><a href="https://open.spotify.com/artist/5vFyh7GL35ShoJWrXL9aUR?si=NSvHVlZ1TmGNy5vRS-q0yQ" target="_blank"><img src="/assets/images/icon01.png" alt="spotify"/></a></li>
        <li><a href="https://itunes.apple.com/jp/artist/mono-no-aware/1204011959" target="_blank"><img src="/assets/images/icon02.png" alt="apple music"/></a></li>
        <li><a href="https://www.instagram.com/mono.no.aware.0630/" target="_blank"><img src="/assets/images/icon03.png" alt="instagram"/></a></li>
        <li><a href="https://twitter.com/mono_no_aware_" target="_blank"><img src="/assets/images/icon04.png" alt="twitter"/></a></li>
        <li><a href="https://www.youtube.com/user/spaceshowermusic" target="_blank"><img src="/assets/images/icon05.png" alt="youtube"/></a></li>
      </ul>
  </nav>
	</div>
</header>
 
  <section id="live" class="sec04">
    <div class="inner">
      <h2><img src="/assets/images/ttl_icon_live.svg" alt="LIVE"/></h2>
      <ul>
        <li><iframe width="560" height="315" src="https://www.youtube.com/embed/UzACD_m_ZfA?rel=0&amp;controls=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></li>
		  <li><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/BzJGdIlUOdc?rel=0&amp;controls=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></li>
		  <li><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/UzvRfNicSgY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></li>
		  <li><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/oqu96zlXLq4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></li>
		  <li><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/KcHgQ52iExw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></li>
      </ul>
    </div>
  </section>

		<?php get_footer(); ?>