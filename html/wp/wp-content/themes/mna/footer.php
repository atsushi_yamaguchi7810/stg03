<footer>
  <ul class="snsArea">
    <li><a href="https://open.spotify.com/artist/5vFyh7GL35ShoJWrXL9aUR?si=NSvHVlZ1TmGNy5vRS-q0yQ" target="_blank"><img src="/assets/images/icon01.png" alt="spotify"/></a></li>
    <li><a href="https://itunes.apple.com/jp/artist/mono-no-aware/1204011959" target="_blank"><img src="/assets/images/icon02.png" alt="apple music"/></a></li>
    <li><a href="https://www.instagram.com/mono.no.aware.0630/" target="_blank"><img src="/assets/images/icon03.png" alt="instagram"/></a></li>
    <li><a href="https://twitter.com/mono_no_aware_" target="_blank"><img src="/assets/images/icon04.png" alt="twitter"/></a></li>
    <li><a href="https://www.youtube.com/user/spaceshowermusic" target="_blank"><img src="/assets/images/icon05.png" alt="youtube"/></a></li>
  </ul>
  <p>&copy; 2016–2018 - MONO NO AWARE</p>
</footer>

<div id="page-top">
  <div id="linkPagetop"> <a href="#head">PAGE TOP</a> </div>
</div>
	</div>
<?php wp_footer(); ?>
</body>
</html>