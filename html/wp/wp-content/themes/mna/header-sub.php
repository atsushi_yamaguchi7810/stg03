<!doctype html>
<html class="no-js" lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width">
<meta property="og:title" content="MONO NO AWAREの公式ホームページ">
<meta property="og:type" content="website">
<meta property="og:url" content="http://mono-no-aware.jp/">
<meta property="og:image" content="http://mono-no-aware.jp/assets/images/ogp.jpg">
<meta property="og:site_name" content="<?php the_title(); ?>｜MONO NO AWARE OFFICIAL SITE">
<meta property="og:description" content="MONO NO AWARE OFFICIAL SITE">
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@mono_no_aware_" />
<meta name="twitter:player" content="http://mono-no-aware.jp/" />
<meta name="keywords" content="MONO NO AWARE,モノノアワレ">
<meta name="description" content="MONO NO AWAREの公式ホームページ">
<title><?php the_title(); ?>｜MONO NO AWARE OFFICIAL SITE</title>
<link rel="shortcut icon" href="/favicon.ico">
<link href="/assets/css/style.css" rel="stylesheet">
<script src="/assets/js/jquery-2.2.4.min.js"></script>
<script src="/assets/js/slidebars.min.js"></script>
<script src="/assets/js/aos.js"></script>
<script src="/assets/js/script.js"></script>
<script type="text/javascript" src="//typesquare.com/3/tsst/script/ja/typesquare.js?Apozj4GY7HU%3D" charset="utf-8"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122365823-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-122365823-1');
</script>

<?php wp_head(); ?>
</head>